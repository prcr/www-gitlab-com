---
layout: markdown_page
title: "GitLab to Enable Cloud Native Transformation on VMware Cloud Marketplace"
---

_Development teams increase the velocity of innovation as well as control deployment budgets_

**BROOKLYN, NY — [GitLab Commit](https://about.gitlab.com/events/commit/) — September 17, 2019 —** [GitLab](https://about.gitlab.com/), the DevOps platform delivered as a single application,  has announced the GitLab Enterprise (Core) solution is now published and available for use on the newly-launched [VMware Cloud Marketplace](https://marketplace.cloud.vmware.com/services/details/129dc4e9-191d-405f-ab4d-803d56f366a9), helping customers take advantage of the simplicity and enhanced velocity of software innovation that GitLab delivers.

Development teams will now be able to deploy and run GitLab Enterprise (Core) on their VMware environments in a few clicks. GitLab is packaged and supported by [Bitnami](https://bitnami.com/) which provides curated applications for VMware’s marketplace.  

With everything from issue tracking and source code management to CI/CD and monitoring in one place, GitLab simplifies toolchain complexity and deals with bottlenecks, so developers can respond flexibly to their organization’s changing demands. With a built-in container registry and Kubernetes integration, GitLab makes it easier for teams to start using containers and cloud native development and to optimize app development processes.

“We are excited to work with VMware to help organizations accelerate their cloud native transformation,'' said Brandon Jung, VP of Alliances at GitLab. “Organizations can now empower their developers to innovate faster without waiting for infrastructure approvals and provisioning.”

GitLab’s work with VMware’s concept of [“Continuous Verification”](https://thenewstack.io/how-continuous-security-can-solve-the-cloud-protection-conundrum/) makes it easier for organizations to establish and control infrastructure through security, monitoring, and budget analysis while giving developers freedom to innovate within clearly-defined policy and budget controls. Continuous Verification shows how Gitlab seamlessly works with [VMware Secure State](https://go.cloudhealthtech.com/vmware-secure-state), [Wavefront by VMware](https://cloud.vmware.com/wavefront), and [CloudHealth](https://www.cloudhealthtech.com/). Through these offerings, organizations are able to increase their operational efficiencies while enabling developers to deliver better products faster.

“We are pleased to see GitLab available on VMware Cloud Marketplace,” said Milin Desai, GM, Cloud Services, VMware. “Enabling automated deployments with verified Bitnami images and codified policies enables a Continuous Verification process which can reduce costs, security risks, and potential performance issues. We’re excited to work with partners such as GitLab to empower customers to fully leverage their cloud investments.”

To learn more about GitLab’s integration into VMware’s concept of Continuous Verification, read [How to prevent deployments from overrunning your budget](https://about.gitlab.com/2019/08/26/cloudhealth-and-gitlab-reducing-overruns/) by Bahubali (Bill) Shetti is the director of public cloud solutions for VMware Cloud Services at VMware.

#### About GitLab
GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity. GitLab provides unmatched visibility, radical new levels of efficiency and comprehensive governance to significantly compress the time between planning a change and monitoring its effect. Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network and Comcast trust GitLab to deliver great software at new speeds.

_VMware, VMware Cloud and VMware Cloud Marketplace are registered trademarks or trademarks of VMware, Inc. in the United States and other jurisdictions._

#### Media Contact
Natasha Woods
<br>
GitLab
<br>
nwoods@gitlab.com
<br>
(415) 312-5289
